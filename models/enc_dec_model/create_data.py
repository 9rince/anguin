import os

path = os.path.join(os.getcwd(),'models','enc_dec_model')
os.chdir(path)

import pandas as pd
import re
# from textblob import TextBlob

corpus_name = "cornell_movie_quotes_corpus"
corpus = os.path.join("./data", corpus_name)

def create_data(corpus):
    
    data_file = open('data.txt','w')
    fn = os.path.join(corpus, "moviequotes.scripts.txt")
    with open(fn,'rb') as file:
        lines = file.readlines()
    
    for i in range(1,len(lines)):
        try:
            data_1 = str(lines[i-1], 'utf-8').split("+++$+++")
            data_2 = str(lines[i], 'utf-8').split("+++$+++")
            if data_1[1]==data_2[1]:
                data_file.write(data_1[-1].strip()+'+++'+data_2[-1].strip()+'\n')
            else:
                continue
        except:
            continue
        
create_data(corpus)
print('data created and saved to data.txt')